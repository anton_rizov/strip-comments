#!/usr/bin/perl

use strict;
use warnings;

my $in_block_comment = 0;

while (<>) {
 AGAIN:
  if ($in_block_comment) {
	next unless s{^(.*?)\*/}{};

	$in_block_comment = 0;
  }

  if (s{^(.*?)("|//|/\*)}{}) {
	print $1;

	my $c = $2;

	if ($c eq '//') {
      print "\n";
      next;
	}
    if ($c eq '/*') {
      print " ";
      $in_block_comment = 1;
	} elsif (s/^(?:[^"\\]|(?:\\.))*"//p) {
      print $c;
      print ${^MATCH};
	} else {
      die "string literal not closed";
	}
	goto AGAIN;
  }

  print;
}
