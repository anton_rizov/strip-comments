package com.galileev;

%%
%class CommentStripper
%unicode

%standalone


LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

Comment 	     = {TraditionalComment} | {EndOfLineComment}
TraditionalComment   = "/*"  ~"*/" 
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}


%state STRING

%%
<YYINITIAL> {
  \"				 { System.out.print(yytext()); yybegin(STRING); }

  {EndOfLineComment}		 { System.out.println(); }
  {TraditionalComment}		 { System.out.print(" "); }

  .				 { System.out.print(yytext()); }
}

<STRING> {
  \"                             { System.out.print(yytext()); yybegin(YYINITIAL); }
  [^\"]+                   	 { System.out.print(yytext()); }
  \\\"				 { System.out.print(yytext()); }
}

/* error fallback */
.                                { throw new Error("Illegal character <"+
                                                    yytext()+">"); }
