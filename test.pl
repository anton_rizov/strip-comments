#!/usr/bin/perl

use strict;
use warnings;

use File::Temp qw/tempfile/;

my(@in, @out);
my $name;
my $prog = shift @ARGV;
#my $prog = "java -cp target/strip-comments-1.0-SNAPSHOT.jar com.galileev.CommentStripper";
#my $prog = "perl strip-comments.pl";

sub run_test {
  return unless @in;

  my ($fh, $filename) = tempfile();

  map { print $fh "$_\n" } @in;
  $fh->flush;

  my $result = "ok $name";

  open(my $ps, "-|", "$prog '$filename'") || die "";

  while (my $actual = <$ps>) {
	my $expected = shift @out;
	if ($actual ne $expected) {
      $result = "not $result\n"
		. "\texpected: <$expected>\n"
		. "\t  actual: <$actual>";
      last;
	}
  }

  print "$result\n";

  close $fh;

}

sub copy_line {
  my @line = split //, shift;
  my @spec = split //, shift;

  my $result = "";
  for (my $i = 0; $i < @spec; ++$i) {
	if ($spec[$i] eq '-') {
      $result .= $line[$i];
	} elsif ($spec[$i] eq '^') {
      $result .= ' ';
	}
  }
  return "$result\n";
}

my $n = 0;
while (<>) {
  chomp;
  if (/^Test: (.*)/) {
	run_test;
	$name = $1;
	@in = ();
	@out = ();
	$n = 0;
  } elsif (++$n & 1) {
	push @in, $_;
  } else {
	push @out, copy_line($in[$#in], $_) unless /^\.$/;
  }
}
run_test;
